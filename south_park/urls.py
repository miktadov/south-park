from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings

from apps.home import views as home
from apps.services import views as services
from apps.about import views as about
from apps.gallery import views as gallery
from apps.contact import views as contact
from apps.blog import views as blog


urlpatterns = [
    path('admin/', admin.site.urls),

    path("", home.home_page, name="home_page"),
    path("services/", services.services_page, name="services_page"),
    path("about/", about.about_page, name="about_page"),
    path("gallery/", gallery.gallery_page, name="gallery_page"),
    path("blog/", blog.blog_page, name="blog_page"),
    path("post/<int:id>/", blog.post_view, name="post_view"),
    path("comment/send/<int:id>/", blog.send_comment, name="send_comment"),
    path("contact/", contact.contact_page, name="contact_page"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
