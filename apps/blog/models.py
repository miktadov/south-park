from django.db import models


class BlogPage(models.Model):
    title = models.CharField(max_length=32, default="Блог")
    description = models.CharField(max_length=255, default="")
    image = models.ImageField(upload_to="images/blog/main/")

    def __str__(self) -> str:
        return self.title


class Author(models.Model):
    full_name = models.CharField(max_length=128)
    image = models.ImageField(upload_to="images/blog/authors/")
    description = models.CharField(max_length=255)

    def __str__(self) -> str:
        return self.full_name


class Tag(models.Model):
    title = models.CharField(max_length=32)

    def __str__(self) -> str:
        return self.title


class Post(models.Model):
    image = models.ImageField(upload_to="images/blog/")
    date = models.DateField()
    title = models.CharField(max_length=32)
    description = models.TextField()

    tags = models.ManyToManyField(Tag, blank=True)
    author = models.ForeignKey(Author, on_delete=models.SET_NULL, null=True)

    def __str__(self) -> str:
        return self.title


class Comment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    iamge = models.ImageField(upload_to="images/blog/commentators_images/", blank=True, null=True)
    email = models.EmailField()
    name = models.CharField(max_length=64)
    site = models.URLField(blank=True, null=True)
    message = models.CharField(max_length=1024)

    def __str__(self) -> str:
        return self.name

