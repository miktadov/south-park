from django.contrib import admin

from .models import Post, BlogPage, Author, Tag, Comment


admin.site.register(BlogPage)
admin.site.register(Post)
admin.site.register(Author)
admin.site.register(Tag)
admin.site.register(Comment)
