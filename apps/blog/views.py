from django.shortcuts import render, redirect
from apps.utils import get_base_info
from .models import BlogPage, Post, Comment


def blog_page(request):
    per_page = 12
    page = int(request.GET.get("page", 1))
    posts = Post.objects.all()
    posts_count = posts.count()
    context = {
        "page_name": "Блог",
        "base_block": BlogPage.objects.last(),
        "posts": posts.order_by("date")[(page - 1) * per_page: page * per_page],
        "pages": {_p: _p == page for _p in range(1, (posts_count // per_page) + (2 if posts_count % per_page else 1))},
    }
    context.update(get_base_info())
    return render(request, "pages/blog.html", context)

def post_view(request, id):
    context = {
        "page_name": "Блог",
        "post": Post.objects.get(id=id)
    }
    context.update(get_base_info())
    return render(request, "pages/blog-single.html", context)

def send_comment(request, id):
    post = Post.objects.get(id=id)
    comment = Comment.objects.create(
        name=request.POST.get("name"),
        email=request.POST.get("email"),
        site=request.POST.get("website"),
        message=request.POST.get("message"),
        post=post
    )
    return redirect("post_view", id)