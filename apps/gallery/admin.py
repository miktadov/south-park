from django.contrib import admin
from .models import GalleryPage, Image


admin.site.register(GalleryPage)
admin.site.register(Image)
