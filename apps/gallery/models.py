from django.db import models


class GalleryPage(models.Model):
    title = models.CharField(max_length=32, default="Галлерея")
    description = models.CharField(max_length=255, default="")
    image = models.ImageField(upload_to="images/gallery/main/")

class Image(models.Model):
    title = models.CharField(max_length=16)
    subtitle = models.CharField(max_length=64)
    image = models.ImageField(upload_to="images/gallery/")

