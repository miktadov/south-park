from django.shortcuts import render
from apps.utils import get_base_info
from .models import GalleryPage, Image


def gallery_page(request):
    context = {
        "page_name": "Галлерея",
        "base_block": GalleryPage.objects.last(),
        "images": Image.objects.all()
    }
    context.update(get_base_info())
    return render(request, "pages/gallery.html", context)