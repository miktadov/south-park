from django.db import models


class Service(models.Model):
    icon = models.CharField(max_length=32, default="")
    title = models.CharField(max_length=32, default="")
    description = models.CharField(max_length=255, default="")
    show = models.BooleanField(default=True)


class ServicePage(models.Model):
    title = models.CharField(max_length=32, default="")
    description = models.CharField(max_length=255, default="")
    image = models.ImageField(upload_to="images/service/")
