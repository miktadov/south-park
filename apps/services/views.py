from django.shortcuts import render

from .models import ServicePage, Service
from apps.utils import get_base_info

def services_page(request):
    context = {
        "page_name": "Услуги",
        "base_block": ServicePage.objects.last(),
        "services": Service.objects.filter(show=True),
    }
    context.update(get_base_info())
    return render(request, "pages/services.html", context)