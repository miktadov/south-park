from django.contrib import admin

from .models import Service, ServicePage


admin.site.register(Service)
admin.site.register(ServicePage)
