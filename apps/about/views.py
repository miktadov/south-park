from django.shortcuts import render

from apps.about.models import Review
from .models import AboutPage
from apps.utils import get_base_info

def about_page(request):
    context = {
        "page_name": "О Нас",
        "base_block": AboutPage.objects.last(),
        "reviews": Review.objects.all(),
    }
    context.update(get_base_info())
    return render(request, "pages/about.html", context)
