from django.db import models


class AboutPage(models.Model):
    title = models.CharField(max_length=32, default="О Нас")
    description = models.CharField(max_length=255, default="")
    image = models.ImageField(upload_to="images/about/")


class Review(models.Model):
    image = models.ImageField(upload_to="images/about/reviews/")
    review = models.CharField(max_length=255, default="")
    name = models.CharField(max_length=32)
    position = models.CharField(max_length=32)


class Contacts(models.Model):
    phone = models.CharField(max_length=16, default="")
    email = models.EmailField()
    address = models.CharField(max_length=255, default="")
    wa = models.CharField(max_length=64, default="")
    tg = models.CharField(max_length=64, default="")
