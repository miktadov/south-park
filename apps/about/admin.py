from django.contrib import admin

from .models import Contacts, AboutPage, Review


admin.site.register(AboutPage)
admin.site.register(Review)
admin.site.register(Contacts)
