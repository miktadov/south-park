from django.db import models


class HomePage(models.Model):
    slogan = models.CharField(max_length=128, default="")
    title = models.CharField(max_length=128, default="")
    subtitle = models.CharField(max_length=128, default="")
    image = models.ImageField(upload_to="images/home/")
