from django.shortcuts import render

from .models import HomePage
from apps.services.models import ServicePage, Service
from apps.about.models import Review
from apps.blog.models import Post

from apps.utils import get_base_info


def home_page(request):
    context = {
        "page_name": "Главная",
        "home_page": HomePage.objects.last(),
        
        "service_page": ServicePage.objects.last(),
        "services": Service.objects.filter(show=True),
        "reviews": Review.objects.all(),
        "posts": Post.objects.all()[:3]
    }
    context.update(get_base_info())
    return render(request, "index.html", context)
