from django.db import models


class ContactPage(models.Model):
    title = models.CharField(max_length=32, default="Контакты")
    description = models.CharField(max_length=255, default="")
    image = models.ImageField(upload_to="images/contact/")
