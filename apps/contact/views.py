from django.shortcuts import render

from .models import ContactPage
from apps.utils import get_base_info
from apps.about.models import Contacts

def contact_page(request):
    context = {
        "page_name": "Контакты",
        "base_block": ContactPage.objects.last(),
    }
    context.update(get_base_info())
    return render(request, "pages/contact.html", context)
