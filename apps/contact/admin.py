from django.contrib import admin

from .models import ContactPage

admin.site.register(ContactPage)
