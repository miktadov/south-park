from apps.about.models import Contacts

def get_base_info():
    return {
        "base_info": {
            "contacts": Contacts.objects.last()
        }
    }